import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-editor-component',
  templateUrl: './editor-component.component.html',
  styleUrls: ['./editor-component.component.css']
})
export class EditorComponentComponent implements OnInit {
  @Output() changeEditorState = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  closeEditor(){
    this.changeEditorState.emit(false);
  }

}
