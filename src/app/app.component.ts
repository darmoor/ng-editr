import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showAppEditon: Boolean = false;

  handleChangeEditorState(state: Boolean){
    this.showAppEditon = state;
  }
}
