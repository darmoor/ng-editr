import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-final-viewer-component',
  templateUrl: './final-viewer-component.component.html',
  styleUrls: ['./final-viewer-component.component.css']
})
export class FinalViewerComponentComponent implements OnInit {
  @Output() changeEditorState = new EventEmitter();


  constructor() { }

  ngOnInit() {
  }

  openEditor(){
    this.changeEditorState.emit(true);
  }

}
