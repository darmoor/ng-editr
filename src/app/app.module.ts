import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { FinalViewerComponentComponent } from './final-viewer-component/final-viewer-component.component';
import { EditorComponentComponent } from './editor-component/editor-component.component';
import { LeftMenuComponentComponent } from './left-menu-component/left-menu-component.component';
import { TopMenuComponentComponent } from './top-menu-component/top-menu-component.component';
import { MainViewComponentComponent } from './main-view-component/main-view-component.component';
import { ZoomComponentComponent } from './zoom-component/zoom-component.component';

@NgModule({
  declarations: [
    AppComponent,
    FinalViewerComponentComponent,
    EditorComponentComponent,
    LeftMenuComponentComponent,
    TopMenuComponentComponent,
    MainViewComponentComponent,
    ZoomComponentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
