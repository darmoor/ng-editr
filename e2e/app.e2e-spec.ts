import { EditRPage } from './app.po';

describe('edit-r App', function() {
  let page: EditRPage;

  beforeEach(() => {
    page = new EditRPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
